# Paths and prefs
export PATH=$HOME/bin:$PATH
export EDITOR=ee

# locale stuff
export LANG=en_US.UTF-8

# pip bash completion start
_pip_completion() {
    COMPREPLY=( $( COMP_WORDS="${COMP_WORDS[*]}" \
                   COMP_CWORD=$COMP_CWORD \
                    PIP_AUTO_COMPLETE=1 $1 ) )
}
complete -o default -F _pip_completion pip
# pip bash completion end

# The colors, duke! The colors!
export TERM=xterm-256color

function color () {	echo -ne "\[\033[38;5;$1m\]";            }
function no_color {	echo -n "\\[\\e[0m\\]";                     }
function bold {		echo -n "\\[\\033[1m\\]"$1"\\[\\033[0m\\]"; }
function bg {		echo -n "\\[\\033[48;5;"$1"m\\]";           }
function fg {		echo -n "\\[\\033[38;5;"$1"m\\]";           }
function prompt_char {
    git branch >/dev/null 2>/dev/null && echo '±' && return;
    hg root >/dev/null 2>/dev/null && echo '☿' && return;
    echo '○';
}

function fancyprompt {
    case "$HOSTNAME" in 
        magenta*)
            PROMPT_COLOR=162
        ;;
        indigo*)
            PROMPT_COLOR=93
        ;;
        scarlet*)
            PROMPT_COLOR=160
        ;;
        lemon*)
            PROMPT_COLOR=226
	;;
        lime*)
            PROMPT_COLOR=154
        ;;
        rain*)
            PROMPT_COLOR=33
        ;;
        teal*)
            PROMPT_COLOR=87
        ;;
        saffron*)
            PROMPT_COLOR=214
        ;;
        ash*)
            PROMPT_COLOR=238
        ;;
        *)
            PROMPT_COLOR=252
        ;;
    esac
    case "$(whoami)" in
        root|toor)
            PROMPT_COLOR=196
        ;;
    esac
    export PS1="$(fg $PROMPT_COLOR)[\u@\h \w]$(prompt_char) $(no_color)";
}
function dullprompt {	export PS1="[\u@\h \w]\$ ";				}

case "$TERM" in
    xterm-color|xterm-256color|rxvt*|screen-256color)
        fancyprompt
    ;;
    *)
        dullprompt
    ;;
esac

# The "I'm lazy" aliases
case $OSTYPE in
    linux-gnu*)
        alias ls='ls --color=auto'
    ;;
    freebsd*)
        alias ls='ls -G'
    ;;
esac

alias ..='cd ..'
alias ~='cd ~'
#alias .='pwd'
alias unmount='umount'
alias dir='echo "durrhurrhurr"'
alias su='su -l' # To get root prompt color. Making power -obvious-.

# virtualenv and virtualenvwrapper
export WORKON_HOME=~/.virtualenvs
if [ -f "/usr/local/bin/virtualenvwrapper.sh" ]
then
    source /usr/local/bin/virtualenvwrapper.sh
fi

# I've forgotten what the hell this is for
# Oh I think this is line length matching terminal length... I think...
resize >& /dev/null

